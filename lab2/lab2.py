import matplotlib.pyplot as plt
import matplotlib.colors as colors
import re

#функция для конвертирования цветов


def hex_to_rgb(hex_string):
    rgb = colors.hex2color(hex_string)
    return tuple([int(255*x) for x in rgb])

#открытие файла, если открылся, то прочитать


with open('mask5_294x182.html', 'r') as file:
    main = file.read()

#парсинг файла, выбираем из него только цвета HEX

colorHEX = re.findall('#\w\w\w\w\w\w', main)
colorRGB = []
#print(colorHEX)

#преобразование HEX в RGB

for i in colorHEX:
    colorRGB.append(hex_to_rgb(i))
#print(colorRGB)
colorRGBlight = []

#первая часть задания (Преобразование каждого цвета пикселя по формуле)

for i in colorRGB:
    colorRGBlight.append(0.299*i[0]+0.587*i[1]+0.114*i[2])
#print(colorRGBlight)


#вторая часть задания (строим гистограмму по значениям яркости)

label_Y = len(colorRGBlight)
k_min = 0.55*label_Y**0.4
k_max = 1.25*label_Y**0.4
k = int(k_min + k_max) // 2
label_Y, bins, patches = plt.hist(colorRGBlight, bins=k, density=True)
plt.xlabel("Яркость")
plt.ylabel("Частота")
plt.show()

#третья часть задания
# label_Y - y
# bins - x
#  (label_Y[0], bins[0]) = (0,0)
local_mins = []
temp = []
for i in range(1, len(label_Y)-1):
    if label_Y[i] < label_Y[i-1] and label_Y[i] < label_Y[i+1]:
        local_mins.append(bins[i])
    elif label_Y[i] <= label_Y[i-1] and label_Y[i] <= label_Y[i+1]:
        temp.append(bins[i])
    elif temp:
        local_mins.append(sum(temp)/len(temp))
        temp = []

print(local_mins, len(local_mins))


#четвертая часть задания (заменяем пиксели на красный, во второй по частоте области однородности)

for i in range(len(colorRGBlight)):
    if colorRGBlight[i] > local_mins[6]:
        colorRGBlight[i] = 255*0.299
        colorRGB[i] = [255, 0, 0]





#пятая часть задания (собираем все вместе и записываем в HTML-таблицу)
tr_count = len(re.findall('/TR', main))
td_count = int(len(colorRGBlight)/tr_count)

result_table = ""
count = 0
for i in range(tr_count):
    tr = ""
    for j in range(td_count):
        tr += "<TD style=\"background-color: rgb({0}, {1}, {2});\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>".format(
            colorRGB[count][0], colorRGB[count][1], colorRGB[count][2]
            )
        count += 1
    result_table += "<TR>{0}</TR>".format(tr)

result_file = "<HTML><BODY><TABLE>{0}</TABLE></BODY></HTML>".format(result_table)

fileout = open("plane.html", "w")
fileout.writelines(result_file)
fileout.close()
