import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
data = pd.read_csv("C:/dataset/android_traffic1.csv", delimiter=';')
print(data)
n = len(data['tcp_packets'])
k_min = 0.55*n**0.4
k_max = 1.25*n**0.4
print(k_min, k_max)
k = 43
plt.figure()

d = plt.hist(data['tcp_packets'], bins = k, density= True)
#sns.distplot(data['tcp_packets'])
plt.xlabel("%CPU")
plt.ylabel("time")
sns.distplot()
plt.show()

mean = np.mean(data['tcp_packets'])
print(mean, 'Математическое ожидание')

#median = np.median(data['URL_LENGTH'])
#print(median,'Медианой [серединным значением]')

#var = np.var(data['URL_LENGTH'])
#print(var,'Дисперсия случайной величины')

std = np.std(data['tcp_packets'])
print(std, 'Средним квадратическим отклонением')